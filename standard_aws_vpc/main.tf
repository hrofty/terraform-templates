provider "aws" {
  region = "${var.region}"
}

resource "aws_vpc" "VPC" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"
  tags = {
    Name    = "${var.project}_VPC"
    Project = "${var.project}"
  }
}

resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.VPC.id
  tags = {
    Name    = "${var.project}_Internet_Gateway"
    Project = "${var.project}"
  }
}

#--------------route tables------------------

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.VPC.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.IGW.id
  }
  tags = {
    Name = "${var.project}_Public_Route_Table"
    Project = "${var.project}"
  }
}

resource "aws_route_table" "private_route_table" {
  count = var.zones
  vpc_id = aws_vpc.VPC.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.NGW[count.index].id
  } 
  tags = {
    Name = "${var.project}_Private_Route_Table_${data.aws_availability_zones.available.names[count.index]}"
    Project = "${var.project}"
  }
}

resource "aws_route_table" "db_route_table" {
  vpc_id = aws_vpc.VPC.id
  route = []
  tags = {
    Name = "${var.project}_Database_Route_Table"
    Project = "${var.project}"
  }
}

resource "aws_eip" "EIP" {
  count = var.zones

  tags = {
    Name = "${var.project}_EIP_for_NAT_Gateway_${data.aws_availability_zones.available.names[count.index]}"
    Project = "${var.project}"
  }
}

resource "aws_nat_gateway" "NGW" {
  count = var.zones
  allocation_id = aws_eip.EIP[count.index].id
  subnet_id     = aws_subnet.public_subnets[count.index].id

  tags = {
    Name = "${var.project}_NAT_Gateway_${data.aws_availability_zones.available.names[count.index]}"
    Project = "${var.project}"
  }

}

#----------subnets------------

data "aws_availability_zones" "available" {
  state = "available"
}



resource "aws_subnet" "public_subnets" {
  count = var.zones
  vpc_id     = aws_vpc.VPC.id
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block = "10.0.${(count.index + 1) * 10}.0/24"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.project}_Public_Subnet_${data.aws_availability_zones.available.names[count.index]}"
    Project = "${var.project}"
  }
}



resource "aws_subnet" "private_subnets" {
  count = var.zones
  vpc_id     = aws_vpc.VPC.id
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block = "10.0.${((count.index + 1) * 10) + 1}.0/24"
  map_public_ip_on_launch = false
  tags = {
    Name = "${var.project}_Private_Subnet_${data.aws_availability_zones.available.names[count.index]}"
    Project = "${var.project}"
  }
}

resource "aws_subnet" "db_subnets" {
  count = var.zones
  vpc_id     = aws_vpc.VPC.id
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block = "10.0.${((count.index + 1) * 10) + 2}.0/24"
  map_public_ip_on_launch = false
  tags = {
    Name = "${var.project}_Database_Subnet_${data.aws_availability_zones.available.names[count.index]}"
    Project = "${var.project}"
  }
}


resource "aws_route_table_association" "public" {
  count = var.zones
  subnet_id      = aws_subnet.public_subnets[count.index].id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table_association" "private" {
  count = var.zones
  subnet_id      = aws_subnet.private_subnets[count.index].id
  route_table_id = aws_route_table.private_route_table[count.index].id
}

resource "aws_route_table_association" "db" {
  count = var.zones
  subnet_id      = aws_subnet.db_subnets[count.index].id
  route_table_id = aws_route_table.db_route_table.id
}



