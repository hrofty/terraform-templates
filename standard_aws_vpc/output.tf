output "VPC_CIDR" {
  value = aws_vpc.VPC.cidr_block
}

output "EIPs" {
  value = aws_eip.EIP[*].public_ip
}
