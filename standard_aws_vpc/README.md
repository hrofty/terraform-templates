This is my basic AWS VPC template. I have launched 4 small projects using it as starting point.
It creates VPC and adds 3 subnets for every availability zone set:
* Public - with public IP addresses
* Privat - behind NAT
* Database - with no internet access

You need to specify AWS region, number of availability zones and project name.
