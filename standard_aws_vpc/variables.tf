variable "region" {
  type = string
  default = "us-east-1"
  description = "Region to deploy to"
}

variable "zones" {
  type = number
  default = 2
  description = "Number of availability zones used"
}

variable "project" {
  type = string
  default = "Phoenix"
  description = "Project name to tag resources"
}
